import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

//Teste kjøp ved bruk av betalingsinformasjon
Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
  cy.visit("http://localhost:8080");

  cy.get("#product").select("Hubba bubba");
  cy.get("#quantity").clear().type("4");
  cy.get("#saveItem").click();
});

And(/^trykket på Gå til betaling$/, () => {
  cy.get("#goToPayment").click();
});

When(/^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/, () => {
    cy.get("#fullName").clear().type("test").blur();
    cy.get("#address").clear().type("test").blur();
    cy.get("#postCode").clear().type("test").blur();
    cy.get("#city").clear().type("test").blur();
    cy.get("#creditCardNo").clear().type("1234512345123451").blur();

});

And(/^trykker på Fullfør kjøp$/, () => {
  cy.contains("Fullfør handel").click();
});

Then(/^skal jeg få beskjed om at kjøpet er registrert$/, function () {
  cy.get(".confirmation").should("exist");
});




//Teste betalingsinformasjon
Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
  cy.visit("http://localhost:8080");
});

And(/^trykket på Gå til betaling$/, () => {
  cy.get("#product").select("Hubba bubba");
});

When(/^jeg legger inn ugyldige verdier i feltene$/, () => {
  cy.get("#fullName").clear().blur();
  cy.get("#address").clear().blur();
  cy.get("#postCode").clear().blur();
  cy.get("#city").clear().blur();
  cy.get("#creditCardNo").clear().blur();
  cy.contains("Fullfør handel").click();
});

Then(/^skal jeg få feilmeldinger for disse$/, function () {
   cy.get("#fullNameError").should("have.text", "Feltet må ha en verdi");
   cy.get("#addressError").should("have.text", "Feltet må ha en verdi");
   cy.get("#postCodeError").should("have.text", "Feltet må ha en verdi");
   cy.get("#cityError").should("have.text", "Feltet må ha en verdi");
   cy.get("#creditCardNoError").should("have.text", "Kredittkortnummeret må bestå av 16 siffer");
});
